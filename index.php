<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

        require __DIR__ . '/vendor/autoload.php';

        use Cloudinary\Configuration\Configuration;
        use Cloudinary\Api\Upload\UploadApi;

        Configuration::instance('cloudinary://2133556712893122:m2IHdiyDaYdoi362jEgUJWhXDvEE@dkg5lwz3cc');
      
        if(isset($_POST['submit']))
        {
            $upload = new UploadApi();
            $filename = $_FILES['image']['tmp_name'];
            $name = $_FILES['image']['name'];
            echo json_encode(
                $upload->upload($filename, [
                    'public_id' => $name,
                    'use_filename' => TRUE,
                    'overwrite' => TRUE]),
                JSON_PRETTY_PRINT
            );
        }
    ?>

    <form action="" method="post" enctype='multipart/form-data'>
        <input type="file" name="image" id="image">
        <button name="submit">Submit</button>
    </form>
</body>
</html>